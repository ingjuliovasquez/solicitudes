<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->id();
            $table->string("nombre", 50)->unique();
            $table->string("apellido_paterno", 50);
            $table->string("apellido_materno", 50);
            $table->string("direccion", 150);
            $table->string("telefono",15);
            $table->string("curp", 50);
            $table->string("nss", 13);
            $table->Integer("credito");
            $table->boolean("activo");
            $table->foreignId("id_user");
            $table->timestamps();
        });

        Schema::table('solicitudes', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
