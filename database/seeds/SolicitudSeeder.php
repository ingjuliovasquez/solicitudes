<?php

use Illuminate\Database\Seeder;

class SolicitudSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('solicitudes')->insert([
                        
            ['nombre' => 'Julio', 'apellido_paterno' => 'Vásquez', 'apellido_materno' => 'Díaz','direccion' => 'Av. universidad', 'telefono' =>'2222222' , 'curp' =>'VADJ95HVZSZL09' ,'nss' => 'DSDGF12', 'credito' => '12000', 'activo' =>1 ,'id_user' => 1],
            ['nombre' => 'Meliisa', 'apellido_paterno' => 'Garcia', 'apellido_materno' => 'Hernandez','direccion' => 'Av. CONOCIDA', 'telefono' =>'2222222' , 'curp' =>'VADJ95HVZSZL09' ,'nss' => 'sdfsfg', 'credito' => '14000', 'activo' =>0 ,'id_user' => 1],
            ['nombre' => 'Marcos', 'apellido_paterno' => 'Diaz', 'apellido_materno' => 'Fernandez','direccion' => 'Av. CHIAPAS', 'telefono' =>'2222222' , 'curp' =>'VADJ95HVZSZL09' ,'nss' => 'ADCDD', 'credito' => '16000', 'activo' =>0 ,'id_user' => 1],
            ['nombre' => 'Ana', 'apellido_paterno' => 'Cortez', 'apellido_materno' => 'Diaz','direccion' => 'Av. GHerra', 'telefono' =>'2222222' , 'curp' =>'VADJ95HVZSZL09' ,'nss' => 'DSDGF12', 'credito' => '15000', 'activo' =>0 ,'id_user' => 1],
            ['nombre' => 'Magda', 'apellido_paterno' => 'Galia', 'apellido_materno' => 'Rivera','direccion' => 'Chiapas', 'telefono' =>'2222222' , 'curp' =>'VADJ95HVZSZL09' ,'nss' => 'DSDGF12', 'credito' => '16000', 'activo' =>1 ,'id_user' => 1]
          
        ]);
    }
}
