$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });                       

    
});



$('#guardar').on( 'click',  function () {
    var _token = $('meta[name="csrf-token"]').attr('content')
    // console.log(id);
    
   var nombre = $('#nombre').val();
   var primer_apellido = $('#primer_apellido').val(); 
   var segundo_apellido = $('#segundo_apellido').val(); 
   var direccion = $('#direccion').val(); 
   var telefono = $('#telefono').val(); 
   var curp = $('#curp').val(); 
   var nss = $('#nss').val(); 
   var credito = $('#credito').val(); 

    Swal.fire({
    
        title: "Deseas agregar nueva Solicitud.",
        text: "",
        type: "warning",
       showCancelButton: true,
       confirmButtonClass: "btn-danger",
       confirmButtonText: "Aceptar",
       confirmButtonColor: '#b1262b',
       cancelButtonText: "Cancelar",
       cancelButtonColor: '#333333',
    //
    // allowOutsideClick: () => !Swal.isLoading(),
   
  }).then((result)=>{
    if (result.value) {
        $.ajax({
            type: 'POST',
            data:{
                'nombre' : nombre,
                'primer_apellido' : primer_apellido,
                'segundo_apellido' : segundo_apellido,
                'direccion' : direccion,
                'telefono' : telefono,
                'curp' : curp,
                'nss' : nss,
                'credito' : credito
               
            },
            cache: false,
            url: route('solicitud.store'),
            // dataType: "JSON",
            enctype: 'multipart/form-data',
            success : function(response) {
                if(response.status == 1){
                    Swal.showLoading();
                    Swal.fire('Producto Registrado','Acción realizada con exito','success');
                    // mensaje('Registro Eliminado','Acción realizada con exito','success')
                
                     window.location.href = route('admin');
                }else{
                    Swal.fire('Ocurrio un problema, intente nuevamente','warning')
                }
        
            }
        });
     }

  })
 
});


