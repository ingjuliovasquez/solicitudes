<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Todos los derechos reservados
mexico city
te amo bebe*/
//tecnologia je//

// FFFF
Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'SolicitudController@index')->name('admin');
Route::get('/admin/solicitud', 'SolicitudController@indexSolicitud')->name('admin.solicitud');
Route::post('/admin/solicitud', 'SolicitudController@Solicitud')->name('solicitud.store');
Route::post('/admin/delete', 'SolicitudController@eliminar')->name('eliminar');
Route::post('/admin/editar', 'SolicitudController@editar')->name('editar');

//Rutas para index
Route::get('/admin/solcitudes', 'SolicitudController@indexSolicitudes')->name('admin.index');

Route::get('/admin/tabla/solicitudes', 'SolicitudController@tablaSolicitudes')->name('tabla.solicitudes');