
@extends('template.admin')

@section('content')
<!-- Start content -->
      <!-- Start right Content here -->
            <!-- ============================================================== -->

                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">Solicitudes De Crédito</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                     
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                            
                                               <H3>Cerrar sesión</H3> 
                                            </a>
                            
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>
                        <!-- end page-title -->
                    <div class="row">
                

                        <div class="col-md-6 col-xl-3">
                            <a href="{{ route('admin.solicitud') }}">
                            <div class="card m-b-30">
                                <div class="text-md-center"><br><br>
                                
                                <i class="icon-plus fa-10x"></i>
                               
                                </div>
                                <div class="card-body text-md-center">
                                    
                                    <h4 class="card-title font-26 mt-0 ">Nueva Solicitud </h4>
                                   
                                    <br>
                                </div>
                            
                            </div>
                            </a>
                        </div><!-- end col -->

                        <div class="col-md-6 col-xl-3">
                            <a href="{{ route('admin.index') }}">
                            <div class="card m-b-30">
                                <div class="text-md-center"><br><br>
                                
                                <i class="fa fa-database fa-10x"></i>
                               <br>
                                </div>
                                <div class="card-body text-md-center">
                                    
                                    <h4 class="card-title font-26 mt-0 ">Ver Solicitudes </h4>
                                   
                                    <br>
                                </div>
                            
                            </div>
                            </a>
                        </div><!-- end col -->
                    </div>

                    
                    </div>
                </div>
            </div>

            
            <!-- ============================================================== -->
            <!-- End Right content here -->
<!-- content -->
@endsection
@section('scripts')

        <script>
  

        </script>  

@endsection