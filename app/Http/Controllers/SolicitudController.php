<?php

namespace App\Http\Controllers;
use App\Solicitude;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SolicitudController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function indexSolicitud()
    {
        return view('admin.producto');
    }
    public function Solicitud(Request $request)
    {
            DB::beginTransaction();
            try {
                $user = new Solicitude();
                $user->nombre =$request->nombre;
                $user->apellido_paterno = $request->primer_apellido;
                $user->apellido_materno = $request->segundo_apellido;
                $user->direccion = $request->direccion;
                $user->telefono = $request->telefono;
                $user->curp = $request->curp;
                $user->nss = $request->nss;
                $user->credito = $request->credito;
                $user->id_user = 1;
                $user->save();
            
           
        
                DB::commit();
                // return view('panel.solicitudAdmin');
                    return response()->json(['response' => 'success', 'status' => 1],200);
            } catch (\Throwable $th) {
                DB::rollback();
                // return response()->json(['response' => 'error', 'status' => 3],400);
                return $th;
            }
      

    }

    public function indexSolicitudes()
    {
        return view('admin.solicitudes');
    }


    public function tablaSolicitudes()
    {
      
        $productos = Solicitude::orderBy('created_at','asc')->get();
      
            
        $arr_esc = array();

        
       

        foreach ( $productos as  $producto) {
                 
           if($producto->activo == 1){
       $acciones = '<button type="button" class="btn btn-primary btn-circle" onclick="productoEditar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Editar</button> &nbsp; '.
       '<button type="button" class="btn btn-warning btn-circle" onclick="producto('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Dar de Baja</button> &nbsp; '.
       '<button type="button" class="btn btn-danger  btn-circle "  onclick="productoEliminar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Eliminar</button> &nbsp; ';
           }else{
            $acciones = '<button type="button" class="btn btn-primary btn-circle" onclick="productoEditar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Editar</button> &nbsp; '.
            '<button type="button" class="btn btn-secundary btn-circle" onclick="producto('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Dar de Alta</button> &nbsp; '.
            '<button type="button" class="btn btn-danger  btn-circle "  onclick="productoEliminar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Eliminar</button> &nbsp; ';
           }
        
            array_push($arr_esc, array(
                'id'=>$producto->id,
                'nombre'=>$producto->nombre,
                'apellido_paterno'=>$producto->apellido_paterno. ' ' .$producto->apellido_materno ,
                'direccion'=>$producto->direccion,
                'curp'=>$producto->curp,
                'nss'=>$producto->nss,
                'credito'=>$producto->credito,
                'boton'=>$acciones,

                
            ));

        }
      
        return response()->json(['response' => 'success', 'status' => 1, 'data' => ($arr_esc)],200);
    }
 

    public function eliminar(Request $request)
    {

 
        DB::beginTransaction();
        try {
            $esr = Solicitude::find($request->id);
                     
            $esr->delete();

            DB::commit();
           
             return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['response' => 'success', 'status' => 2, 'error' => $th],200);
        }
        return response()->json(['response' => 'success', 'status' => 3],200);  
    }

    public function editar(Request $request)
    {

 
        DB::beginTransaction();
        try {
            $esr = Solicitude::find($request->id);
            $esr->credito = $request->producto;
            // $esr->user_id = Auth::user()->id;
            $esr->save();

            DB::commit();
           
                return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }
        return response()->json(['response' => 'success', 'status' => 3],200);
    }




}
