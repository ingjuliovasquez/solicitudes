<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitude extends Model
{
    //
    public $table = 'solicitudes';
    public $fillable = [
        'id',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'direccion',
        'telefono',
        'curp',
        'nss',
        'credito',
        'activo',
        'id_user',

    ];
    public $attributes = [
        "activo" => 1,
    ];

    public function user(){
        return $this->belongsTo('App\User', 'id_user', 'id');
    }
}
